$(document).ready(function(){
	var rq;
	var key = 1;
	var start  = false;
	var users = [];
	var access_token = localStorage.getItem("pageToken");
	//var access_token = true;
	var  messageSend = localStorage.getItem("message") ? localStorage.getItem("message") : 'Chào bạn';

	if(getQueryVariable('saveToken'))
	{
		let page_id = getQueryVariable('page_id');
		localStorage.setItem('pageToken',getQueryVariable('saveToken'));
		window.close();
	}
	if(getQueryVariable('message_auto')){
		localStorage.setItem('message',decodeURI(getQueryVariable('message_auto')).replace( /\+/g, ' '));
		window.close();
	}

	if(!access_token || !getQueryVariable('running'))
	{
		return false;
	}else
	{
		$('._2s1y').css({'background':'green'})
	}
	
	
	$(document).on('keyup', function (e) {
		
		if(e.keyCode == 18)
		{
			$(document).find('._4k8w').first().click();
			user_id = getQueryVariable('selected_item_id');
			token = localStorage.getItem("pageToken");
			if(!user_id || !token){
				alert('sai');
				return false;
			}
			sendMessage(user_id,token,$(this));
		}
		
	});

	function sendMessage(fbid,_this)
	{
		rq = $.ajax({
			'url':'https://graph.facebook.com/t_'+fbid+'/messages',
			'method':'POST',
			'data':{'message':messageSend,'access_token':access_token},
			success:function(res){

			}
		});
		//return rq;
	}
	function getQueryVariable(variable)
	{
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	}
	var getParams = function (url) {
	var params = {};
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);
	}
	return params;
};
	function getAllUsersMessage()
	{
		$('._4k8w').each(function(){
			let name;
			name = $(this).find('._4k8x').text();
			users.push(name);
		});
		if(users.length)
			start = true; 
	}
	//if (jQuery.inArray(name, names)!='-1')
	function findUserNew()
	{
		let firstUser = $('._4k8w').first();
		let textName = firstUser.find('._4k8x').text();
		if (jQuery.inArray(textName, users) =='-1')
		{
			firstUser.click();
			user_id = getQueryVariable('selected_item_id');
			
			if(!user_id){
				return false;
			}
			users.push(textName);
			sendMessage(user_id);
		}
	}
	setInterval(function(){
    	if(start)
    		findUserNew();
    	else
    		getAllUsersMessage();
	}, 1000);

});